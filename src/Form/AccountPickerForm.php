<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_account_picker\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\simple_oauth_account_picker\AccountPickerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the form to pick an account for oauth authorize.
 */
class AccountPickerForm extends FormBase {

  /**
   * Construct new instance.
   */
  public function __construct(
    protected EntityStorageInterface $userStorage,
    protected AccountPickerServiceInterface $accountPickerService,
    protected ModuleHandlerInterface $moduleHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get(AccountPickerServiceInterface::class),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_oauth_account_picker_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $accounts = $this->accountPickerService->getAvailableAccounts();

    // Disable cache.
    $form['#cache'] = ['max-age' => 0];

    $form['#attached']['library'][] = 'simple_oauth_account_picker/account_picker';

    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => ['account-picker'],
      ],
    ];

    // Render element for each account.
    foreach ($accounts as $account) {
      $title = $account->getDisplayName();
      $subtitle = $account->getAccountName();

      $isCurrentUser = $this->currentUser()->id() === $account->id();

      $element = [
        'title' => $title,
        'subtitle' => $subtitle,
        'avatarSrc' => NULL,
        'suffix' => $isCurrentUser ? $this->t('Logged in') : $this->t('Logged out'),
      ];

      // Allow modification of $element in other modules.
      $this->moduleHandler->alter('simple_oauth_account_picker_element', $element, $account);

      $form['actions']['acc_' . $account->id()] = $this->buildAccountPickerElement(
        id: $account->getAccountName(),
        title: $element['title'],
        subtitle: $element['subtitle'],
        avatarSrc: $element['avatarSrc'],
        suffix: $element['suffix']->render(),
      );
    }

    // Render 'other account' element.
    $form['actions']['other_account'] = $this->buildAccountPickerElement(
      id: 'other_account',
      title: $this->t('Use a different account')->render(),
      avatarSrc: 'icon:people-circle',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $id = $trigger['#id'];

    // Do nothing if user selected logged in user.
    if ($id === $this->currentUser()->getAccountName()) {
      return;
    }

    // Otherwise logout user.
    if ($this->currentUser()->isAuthenticated()) {
      user_logout();
    }

    if ($id !== 'other_account') {
      $destination = $this->getRequest()->get('destination');
      $this->getRequest()->query->set('destination', $destination . '&username=' . base64_encode($id));
    }

    if ($id === 'other_account') {
      $url = Url::fromRoute('user.login');
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   * Build an account picker element.
   *
   * @param string $id
   *   The ID of the submit element.
   * @param string $title
   *   Title to display.
   * @param string|null $subtitle
   *   Optional subtitle below the title.
   * @param string|null $avatarSrc
   *   If string, renders an image with the given src.
   *   If `icon:people-circle` renders the icon.
   *   If null, renders the first character of the title as the avatar.
   * @param string|null $suffix
   *   Optional suffix to display at the right side.
   */
  protected function buildAccountPickerElement(string $id, string $title, ?string $subtitle = NULL, ?string $avatarSrc = NULL, ?string $suffix = NULL): array {
    $element['root'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'account-picker-element',
      ],
    ];

    $element['root']['container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'account-picker-element__container',
      ],
    ];

    $element['root']['container']['avatar'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'account-picker-element__avatar',
      ],
    ];

    // Render icon as avatar.
    if ($avatarSrc === "icon:people-circle") {
      $element['root']['container']['avatar']['content'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => 'account-picker-element__avatar-content',
        ],
        '#children' => '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16"><path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0"/><path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1"/></svg>',
      ];
    }
    // Render supplied img src as avatar.
    elseif ($avatarSrc) {
      $element['root']['container']['avatar']['content'] = [
        '#type' => 'html_tag',
        '#tag' => 'img',
        '#attributes' => [
          'class' => 'account-picker-element__avatar-content',
          'src' => $avatarSrc,
        ],
      ];
    }
    // Use first letter of title for avatar.
    else {
      $element['root']['container']['avatar']['content'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => 'account-picker-element__avatar-content account-picker-element__avatar-content_text account-picker-element__palette-' . strtolower(substr($title, 0, 1)),
        ],
        '#children' => sprintf('<div>%s</div>', substr($title, 0, 1)),
      ];
    }

    $element['root']['container']['label'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'account-picker-element__label',
      ],
    ];

    $element['root']['container']['label']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'account-picker-element__label-title',
      ],
      '#value' => $title,
    ];

    if ($subtitle && $title !== $subtitle) {
      $element['root']['container']['label']['subtitle'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => 'account-picker-element__label-subtitle',
        ],
        '#value' => $subtitle,
      ];
    }

    if ($suffix) {
      $element['root']['container']['suffix'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => 'account-picker-element__suffix',
        ],
        '#value' => $suffix,
      ];
    }

    $element['root']['container']['submit_' . $id] = [
      '#id' => $id,
      '#type' => 'submit',
      '#value' => $id,
      '#attributes' => [
        'class' => ['account-picker-element__submit'],
      ],
    ];

    return $element;
  }

}
