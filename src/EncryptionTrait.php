<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_account_picker;

use Drupal\Core\Site\Settings;

/**
 * Provides AES-256-CBC encryption / decryption methods.
 */
trait EncryptionTrait {

  /**
   * Encrypt the given string value.
   *
   * Uses the Drupal Hash Salt as the encryption secret.
   */
  protected function encryptString(string $value) {
    $secret = $this->getSecret();
    $cipherMethod = $this->getCipherMethod();

    // Create IV.
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipherMethod));

    $encrypted = openssl_encrypt($value, $cipherMethod, $secret, 0, $iv);

    // Concat IV and encrypted value.
    $result = base64_encode($iv . $encrypted);

    return $result;
  }

  /**
   * Decrypt the given encrypted string.
   *
   * Uses the Drupal Hash Salt as the encryption secret.
   */
  protected function decryptString(string $encryptedString) {
    $secret = $this->getSecret();
    $cipherMethod = $this->getCipherMethod();

    // Split IV and encrypted data.
    $decoded = base64_decode($encryptedString);
    $ivLength = openssl_cipher_iv_length($cipherMethod);
    $iv = substr($decoded, 0, $ivLength);

    // Failsafe.
    if (strlen($iv) !== $ivLength) {
      \Drupal::logger('simple_oauth_account_picker')->warning('Could not decrypt account picker cookie: IV is of incorrect size!');

      return '';
    }

    // Get encrypted value.
    $encrypted = substr($decoded, $ivLength);

    return openssl_decrypt($encrypted, $cipherMethod, $secret, 0, $iv);
  }

  /**
   * Get the secret for encryption / decryption.
   */
  private function getSecret() {
    $base = Settings::getHashSalt();

    return hash('sha256', $base);
  }

  /**
   * Get the cipher method.
   */
  private function getCipherMethod() {
    return 'AES-256-CBC';
  }

}
