<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_account_picker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\encrypt\EncryptionProfileInterface;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * The account picker service.
 */
class AccountPickerService implements AccountPickerServiceInterface {

  use EncryptionTrait;

  /**
   * Construct new instance.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RequestStack $requestStack,
    protected ?EncryptServiceInterface $encrypt = NULL,
    protected ?EncryptionProfileManagerInterface $encryptionProfileManager = NULL,
    #[Autowire(param: 'simple_oauth_account_picker.encryption_profile')]
    protected ?string $encryptionProfileId = NULL,
  ) {}

  /**
   * Set the encrypt service.
   */
  public function setEncrypt(EncryptServiceInterface $encrypt) {
    $this->encrypt = $encrypt;
  }

  /**
   * Set the encryption profile manager.
   */
  public function setEncryptionProfileManager(EncryptionProfileManagerInterface $encryptionProfileManager) {
    $this->encryptionProfileManager = $encryptionProfileManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSavedUids(?Request $request = NULL): array {
    if (!$request) {
      $request = $this->getRequest();
    }

    $savedData = $this->getCookieValue($request);
    if (!$savedData) {
      return [];
    }

    $uids = array_filter(
      explode(',', $savedData),
      fn($v) => (int) $v > 0,
    );

    // If no uids are saved, return an empty array.
    if (empty($uids)) {
      return [];
    }

    // Make sure saved uids actually correspond to real user accounts.
    $result = $this->entityTypeManager->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $uids, 'IN')
      ->execute();

    return array_values($result);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableAccounts(?Request $request = NULL): array {
    $accounts = [];
    $uids = $this->getSavedUids($request);
    $storage = $this->entityTypeManager->getStorage('user');

    // Add current acount.
    if ($this->currentUser->isAuthenticated()) {
      $accounts[] = $storage->load($this->currentUser->id());
    }

    foreach ($uids as $uid) {
      $uid = (int) $uid;

      // Skip current user.
      if ($uid == $this->currentUser->id()) {
        continue;
      }

      if ($account = $storage->load($uid)) {
        $accounts[] = $account;
      }
    }

    return $accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function saveUid(int|string $uid, Request $request, Response $response): void {
    $uids = $this->getSavedUids($request);

    if (in_array($uid, $uids)) {
      return;
    }

    $uids[] = $uid;
    $this->saveCookieValue($request, $response, implode(',', $uids));
  }

  /**
   * Retreives the cookie value.
   */
  protected function getCookieValue(Request $request) {
    $cookieValue = $request->cookies->get(self::COOKIE_NAME, '');
    if (!$cookieValue) {
      return '';
    }

    try {
      // Decrypt the value if a profile is set.
      if ($encryptionProfile = $this->getEncryptionProfile()) {
        return $this->encrypt->decrypt($cookieValue, $encryptionProfile);
      }

      return base64_decode($cookieValue);
    }
    catch (\Exception) {
      // @todo log error.
      return FALSE;
    }
  }

  /**
   * Saves the cookie value.
   */
  protected function saveCookieValue(Request $request, Response $response, string $rawValue) {
    // Encrypt the value if a profile is set.
    if ($encryptionProfile = $this->getEncryptionProfile()) {
      $value = $this->encrypt->encrypt($rawValue, $encryptionProfile);
    }
    else {
      $value = base64_encode($rawValue);
    }

    $cookie = Cookie::create(self::COOKIE_NAME)
      ->withValue($value)
      ->withExpires(time() + 400 * 24 * 60 * 60)
      ->withHttpOnly(TRUE)
      ->withSecure($request->isSecure())
      ->withSameSite(Cookie::SAMESITE_LAX);

    $response->headers->setCookie($cookie);
  }

  /**
   * Get current request.
   */
  protected function getRequest(): Request {
    return $this->requestStack->getCurrentRequest();
  }

  /**
   * Get the configured encryption profile.
   *
   * If NULL, no encryption is used.
   */
  protected function getEncryptionProfile(): ?EncryptionProfileInterface {
    if (!$this->encrypt || !$this->encryptionProfileManager) {
      return NULL;
    }

    if (!$this->encryptionProfileId) {
      return NULL;
    }

    return $this->encryptionProfileManager->getEncryptionProfile($this->encryptionProfileId);
  }

}
