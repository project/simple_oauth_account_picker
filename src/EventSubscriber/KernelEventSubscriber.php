<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_account_picker\EventSubscriber;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\simple_oauth_account_picker\AccountPickerServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This event subscriber listenes on kernel events.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct new instance.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected AccountPickerServiceInterface $accountPickerService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
      KernelEvents::RESPONSE => 'onKernelResponse',
    ];
  }

  /**
   * Handle kernel request event.
   */
  public function onKernelRequest(RequestEvent $event): void {
    $request = $event->getRequest();
    $authorizeUrl = Url::fromRoute('oauth2_token.authorize')->toString();

    // Only handle the /oauth/authorize route.
    if ($request->getPathInfo() !== $authorizeUrl) {
      return;
    }

    // Account was already picked.
    if ($request->get('account_picked') === '1') {
      return;
    }

    $savedUids = $this->accountPickerService->getSavedUids($request);
    $showAccountPicker = $this->currentUser->isAuthenticated() || count($savedUids) > 0;

    // If account picker should be shown, redirect to form,
    // otherwise to the current route again.
    $route = $showAccountPicker ? 'simple_oauth_account_picker.form' : 'oauth2_token.authorize';

    $destination = Url::fromRoute('oauth2_token.authorize', [], [
      'query' => UrlHelper::parse('/?' . $request->getQueryString())['query'],
    ]);

    // Always set the account_picked=1 query param to not have an infinite loop.
    $url = Url::fromRoute($route, [], [
      'query' => ['destination' => $destination->toString() . '&account_picked=1'],
    ]);

    $event->setResponse(
      new RedirectResponse($url->toString()),
    );
  }

  /**
   * Handle kernel response.
   *
   * Checks if a user login has happend for this request.
   * If so, saves the uid.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    $userId = $event->getRequest()->attributes->get('simple_oauth_account_picker_user_login');
    if (is_null($userId)) {
      return;
    }

    $uid = (int) $userId;

    if ($uid > 0) {
      $this->accountPickerService->saveUid($uid, $event->getRequest(), $event->getResponse());
    }
  }

}
