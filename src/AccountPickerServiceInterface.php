<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_account_picker;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines the interface for a account picker service.
 */
interface AccountPickerServiceInterface {

  public const COOKIE_NAME = 'account_picker_ids';

  /**
   * Get array of saved uids from the cookie.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   If request is null, the current request is used.
   *
   * @return string[]
   *   Array of saved uids.
   */
  public function getSavedUids(?Request $request = NULL): array;

  /**
   * Get array of available accounts.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   If request is null, the current request is used.
   *
   * @return \Drupal\user\UserInterface[]
   *   Array of available user accounts.
   */
  public function getAvailableAccounts(?Request $request = NULL): array;

  /**
   * Adds the given uid to the saved uids cookie.
   *
   * @param int|string $uid
   *   The uid to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   Response to set cookie on.
   */
  public function saveUid(int|string $uid, Request $request, Response $response): void;

}
