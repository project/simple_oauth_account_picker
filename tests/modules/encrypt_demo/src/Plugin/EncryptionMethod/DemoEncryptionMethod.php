<?php

declare(strict_types=1);

namespace Drupal\encrypt_demo\Plugin\EncryptionMethod;

use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\encrypt\Attribute\EncryptionMethod;
use Drupal\encrypt\EncryptionMethodInterface;
use Drupal\encrypt\Exception\EncryptException;
use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;

/**
 * Demo encryption method.
 *
 * This is a demo encryption method that is used for testing purposes.
 *
 * @EncryptionMethod(
 *   id = "demo",
 *   title = @Translation("Demo encryption method"),
 *   description = @Translation("Demo encryption method description."),
 *   key_type = {"encryption"},
 *   can_decrypt = TRUE,
 * )
 */
#[EncryptionMethod(
  id: 'demo',
  title: new TranslatableMarkup('Demo encryption method'),
  description: new TranslatableMarkup('Demo encryption method description.'),
  key_type: ['encryption'],
  can_decrypt: TRUE,
)]
class DemoEncryptionMethod extends EncryptionMethodBase implements EncryptionMethodInterface {

  /**
   * {@inheritdoc}
   */
  public function encrypt($text, $key) {
    $secret = $this->getSecret();
    $cipherMethod = $this->getCipherMethod();

    // Create IV.
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipherMethod));

    $encrypted = openssl_encrypt($text, $cipherMethod, $secret, 0, $iv);

    // Concat IV and encrypted value.
    $result = base64_encode($iv . $encrypted);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, $key) {
    $secret = $this->getSecret();
    $cipherMethod = $this->getCipherMethod();

    // Split IV and encrypted data.
    $decoded = base64_decode($text);
    $ivLength = openssl_cipher_iv_length($cipherMethod);
    $iv = substr($decoded, 0, $ivLength);

    // Failsafe.
    if (strlen($iv) !== $ivLength) {
      throw new EncryptException('Could not decrypt account picker cookie: IV is of incorrect size!');
    }

    // Get encrypted value.
    $encrypted = substr($decoded, $ivLength);

    return openssl_decrypt($encrypted, $cipherMethod, $secret, 0, $iv);
  }

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, $key = NULL) {
    return [];
  }

  /**
   * Get the secret for encryption / decryption.
   */
  private function getSecret() {
    $base = Settings::getHashSalt();

    return hash('sha256', $base);
  }

  /**
   * Get the cipher method.
   */
  private function getCipherMethod() {
    return 'AES-256-CBC';
  }

}
