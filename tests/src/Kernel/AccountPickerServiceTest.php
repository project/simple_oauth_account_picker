<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_account_picker\Kernel;

/**
 * Test the account picker service.
 */
class AccountPickerServiceTest extends AccountPickerServiceTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'serialization',
    'simple_oauth',
    'simple_oauth_account_picker',
  ];

}
