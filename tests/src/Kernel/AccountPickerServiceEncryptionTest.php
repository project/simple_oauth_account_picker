<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_account_picker\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\key\Entity\Key;

/**
 * Test the account picker service with encryption.
 */
class AccountPickerServiceEncryptionTest extends AccountPickerServiceTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'serialization',
    'simple_oauth',
    'simple_oauth_account_picker',
    'key',
    'encrypt',
    'encrypt_demo',
  ];

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->setParameter('simple_oauth_account_picker.encryption_profile', 'demo');

    parent::register($container);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $key = Key::create([
      'id' => 'testing_key_128',
      'label' => 'Testing Key 128 bit',
      'key_type' => "encryption",
      'key_type_settings' => ['key_size' => '128'],
      'key_provider' => 'config',
      'key_provider_settings' => ['key_value' => 'mustbesixteenbit'],
    ]);
    $key->save();

    // Create test encryption profiles.
    EncryptionProfile::create([
      'id' => 'demo',
      'label' => 'Test Encryption profile',
      'encryption_method' => 'demo',
      'encryption_key' => $key->id(),
    ])->save();
  }

}
