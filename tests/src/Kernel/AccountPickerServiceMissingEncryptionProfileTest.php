<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_account_picker\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;

/**
 * Test the account picker service with encryption.
 */
class AccountPickerServiceMissingEncryptionProfileTest extends AccountPickerServiceTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'serialization',
    'simple_oauth',
    'simple_oauth_account_picker',
    'key',
    'encrypt',
  ];

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->setParameter('simple_oauth_account_picker.encryption_profile', 'demo');

    parent::register($container);
  }

}
