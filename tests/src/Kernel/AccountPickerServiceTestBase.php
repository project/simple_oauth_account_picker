<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_account_picker\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\simple_oauth_account_picker\AccountPickerServiceInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test the account picker service.
 */
abstract class AccountPickerServiceTestBase extends EntityKernelTestBase {

  /**
   * The account picker service.
   */
  protected AccountPickerServiceInterface $accountPickerService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');

    $this->accountPickerService = $this->container->get(AccountPickerServiceInterface::class);
  }

  /**
   * Test save uuid.
   */
  public function testSaveUid() {
    $uid = '5';

    $request = Request::create('localhost');
    $response = new Response();

    $this->accountPickerService->saveUid($uid, $request, $response);

    $setCookieHeader = $response->headers->get('Set-Cookie');

    $this->assertNotNull($setCookieHeader);

    $cookie = Cookie::fromString($setCookieHeader);

    $this->assertEquals('account_picker_ids', $cookie->getName());
    $this->assertEquals('lax', $cookie->getSameSite());
    $this->assertTrue($cookie->isHttpOnly());
    $this->assertEquals(34560000, $cookie->getMaxAge());
  }

  /**
   * Test get saved uids.
   */
  public function testGetSavedUids() {
    $userOne = $this->drupalCreateUser();
    $userTwo = $this->drupalCreateUser();
    $this->drupalCreateUser();

    $uids = [$userOne->id(), $userTwo->id(), '17'];

    $request = Request::create('localhost');
    $response = new Response();

    foreach ($uids as $uid) {
      $this->accountPickerService->saveUid($uid, $request, $response);

      $cookie = Cookie::fromString($response->headers->get('Set-Cookie'));
      $request->cookies->set('account_picker_ids', urldecode($cookie->getValue()));
    }

    $retrievedUids = $this->accountPickerService->getSavedUids($request);
    $this->assertEquals([$userOne->id(), $userTwo->id()], $retrievedUids);
  }

  /**
   * Test get saved uids when cookie has malformed data.
   */
  public function testGetSavedUidsWithMalformedData() {
    $request = Request::create('localhost');
    $request->cookies->set('account_picker_ids', '_malformed-xyz-value_');

    $retrievedUids = $this->accountPickerService->getSavedUids($request);
    $this->assertEquals([], $retrievedUids);
  }

  /**
   * Test get saved uids with invalid encryption profile.
   */
  public function testGetSavedUidsWithInvalidEncryptionProfile() {
    $request = Request::create('localhost');
    $request->cookies->set('account_picker_ids', '_malformed-xyz-value_');

    $retrievedUids = $this->accountPickerService->getSavedUids($request);
    $this->assertEquals([], $retrievedUids);
  }

  /**
   * Test available accounts.
   */
  public function testGetAvailableAccounts() {
    $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $user3 = $this->drupalCreateUser();
    $this->drupalCreateUser();

    $this->drupalSetCurrentUser($user3);

    $uids = [$user2->id()];

    $request = Request::create('localhost');
    $response = new Response();

    foreach ($uids as $uid) {
      $this->accountPickerService->saveUid($uid, $request, $response);

      $cookie = Cookie::fromString($response->headers->get('Set-Cookie'));
      $request->cookies->set('account_picker_ids', urldecode($cookie->getValue()));
    }

    $accounts = $this->accountPickerService->getAvailableAccounts($request);
    $ids = array_map(fn($acc) => $acc->id(), $accounts);

    $this->assertEquals([$user3->id(), $user2->id()], $ids);
  }

}
