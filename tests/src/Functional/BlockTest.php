<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_account_picker\Functional;

use Drupal\block\BlockInterface;
use Drupal\Tests\block\Functional\AssertBlockAppearsTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Test account picker block.
 */
class BlockTest extends BrowserTestBase {

  use AssertBlockAppearsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'user',
    'simple_oauth_account_picker',
  ];

  /**
   * The account picker block.
   */
  protected BlockInterface $block;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->block = $this->drupalPlaceBlock('simple_oauth_account_picker_block');
  }

  /**
   * Test if block is placed on page.
   */
  public function testBlock() {
    $this->drupalGet('/user/login');

    $this->assertBlockAppears($this->block);
    $this->assertSession()->elementExists('xpath', "//form[@id = 'simple-oauth-account-picker-form']");
  }

}
