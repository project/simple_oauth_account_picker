# Simple OAuth Account Picker

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Hooks](#hooks)
- [Maintainers](#maintainers)

## Introduction

This module adds an account picker when authorizing via the
`Authorization Code Grant.`

Whenever a user hits the `/oauth/authorize` route a google-like account picker
is shown. The user can then select the current account or can choose to log in
with a different account.

The account picker is **skipped** when the user is anonymous and no previous
login was recorded for the account picker.

After each successful login, the `uid` of the account is saved in a special
cookie.
Each account for the uids saved in the cookie are then shown in the account
picker on subsequent authorizations.

## Requirements

The core module requires Drupal 10 and the following contrib modules:

- [Simple OAuth](https://www.drupal.org/project/simple_oauth)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module does not support any configuration options.

## Hooks

The rendered info for each account element in the account picker can be
altered by using the `hook_simple_oauth_account_picker_element_alter()` hook.

```php
/**
 * Alters the element options to render the account in the picker.
 *
 * The $element array has the following properties:
 * - `title`: The title of the element (bold text)
 * - `subtitle`: (optional) Text below the title.
 * - `avatarSrc`: (optional) Path to the avatar image.
 * - `suffix`: (optional) Suffix rendered at the right side.
 *   Defaults to login status.
 */
function hook_simple_oauth_account_picker_element_alter(
  array &$element,
  UserInterface $user
) {
  $element['title'] = 'Account Title';
  $element['subtitle'] = $user->getEmail();
  $element['avatarSrc'] = 'https://placeholder.com/64x64';
}
```

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
