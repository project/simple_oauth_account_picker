<?php

/**
 * @file
 * Provided Hooks.
 */

declare(strict_types=1);

use Drupal\user\UserInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the element options to render the account in the picker.
 *
 * The $element array has the following properties:
 * - `title`: The title of the element (bold text)
 * - `subtitle`: (optional) Text below the title.
 * - `avatarSrc`: (optional) Path to the avatar image.
 * - `suffix`: (optional) Suffix rendered at the right side.
 *   Defaults to login status.
 */
function hook_simple_oauth_account_picker_element_alter(array &$element, UserInterface $user) {
  $element['title'] = 'Account Title';
  $element['subtitle'] = $user->getEmail();
  $element['avatarSrc'] = 'https://placeholder.com/64x64';
}

/**
 * @} End of "addtogroup hooks"
 */
