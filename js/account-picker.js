(function (Drupal) {
  /**
   * Get a query param recursively.
   *
   * If not found on given path, looks for a
   * path set in destination query param.
   *
   * @param {string} key
   * @param {string} path
   */
  function getQueryParamRec(key, path) {
    const search = path.substring(path.lastIndexOf("?"));
    const params = new URLSearchParams(search);

    const val = params.get(key);
    if (val) {
      return val;
    }

    const dest = params.get("destination");
    if (dest) {
      return getQueryParamRec(key, dest);
    }

    return null;
  }

  /**
   * Drupal behavior to sync username from url to login name input.
   */
  Drupal.behaviors.accountPickerUsernameSync = {
    attach(context) {
      once(
        "accountPickerUsernameSync",
        ".user-login-form input[name='name']",
        context,
      ).forEach(
        /**
         * @param {HTMLInputElement} input
         */
        (input) => {
          const path = location.pathname + location.search;
          const encodedUsername = getQueryParamRec("username", path);

          if (encodedUsername) {
            const username = atob(encodedUsername);
            input.value = username;
          }
        },
      );
    },
  };
})(Drupal);
